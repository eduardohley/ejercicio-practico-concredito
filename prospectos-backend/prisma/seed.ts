import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const ROL = {
  ADMINISTRADOR: 'ADMINISTRADOR',
  PROMOTOR_PROSPECTOS: 'PROMOTOR_PROSPECTOS',
  EVALUADOR_PROSPECTOS: 'EVALUADOR_PROSPECTOS',
};

async function main() {
  const roles = [
    { id: ROL.ADMINISTRADOR },
    { id: ROL.PROMOTOR_PROSPECTOS },
    { id: ROL.EVALUADOR_PROSPECTOS },
  ];

  const countRoles = await prisma.rol.count();
  if (countRoles === 0) {
    await prisma.rol.createMany({ data: roles });
  }

  const countUsuarios = await prisma.usuario.count();
  if (countUsuarios === 0) {
    const usurioAdmin = await prisma.usuario.create({
      data: {
        nombre: 'Usuario Administrador',
        username: 'admin',
        password:
          '$2b$10$Q6Sa1.gb8e7lerZz/obLcexQV3pqAFbGdrOlqHU3gj9FB.bq8Glee',
      },
    });
    await prisma.usuarioRol.create({
      data: { usuarioId: usurioAdmin.id, rolId: ROL.ADMINISTRADOR },
    });
  }
  try {
    const usuarioPromotor = await prisma.usuario.create({
      data: {
        nombre: 'Usuario Promotor',
        username: 'promotor',
        password:
          '$2b$10$9DLJ.dBgIhGSbxBQsDgoqOtVunKcVmFqQKknIIEItmbcd1qwDeSAK',
      },
    });
    const usuarioEvaluador = await prisma.usuario.create({
      data: {
        nombre: 'Usuario Evaluador',
        username: 'evaluador',
        password:
          '$2b$10$uhWJtIauz2I5y0S.ljqDxeWAx8./e1uS7exsfEvyKLas5d6/Bpr6e',
      },
    });
    await prisma.usuarioRol.create({
      data: { usuarioId: usuarioPromotor.id, rolId: ROL.PROMOTOR_PROSPECTOS },
    });
    await prisma.usuarioRol.create({
      data: { usuarioId: usuarioEvaluador.id, rolId: ROL.EVALUADOR_PROSPECTOS },
    });
  } catch (error) {}
}

main()
  .catch((e) => console.log(e))
  .finally(() => prisma.$disconnect());

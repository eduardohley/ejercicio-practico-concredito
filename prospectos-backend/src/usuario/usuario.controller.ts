import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Prisma } from '@prisma/client';
import { UsuarioService } from './usuario.service';
import {
  CreateUsuarioRequestDto,
  CreateUsuarioResponseDto,
  GetUsuarioResponseDto,
} from './dto';
import { hashPassword } from '../core/hash/hash-password';
import { ServerException } from '../core/exceptions';
import { Roles } from '../auth/decorators/roles.decorator';
import { Rol } from '../auth/enums/rol.enum';

@ApiTags('Usuario')
@Controller('usuario')
export class UsuarioController {
  constructor(private usuarioService: UsuarioService) {}

  @Get(':id')
  @Roles(Rol.ADMINISTRADOR, Rol.EVALUADOR_PROSPECTOS, Rol.PROMOTOR_PROSPECTOS)
  @ApiBearerAuth()
  @ApiOkResponse({ type: GetUsuarioResponseDto })
  @ApiNotFoundResponse({ type: ServerException })
  @ApiInternalServerErrorResponse({ type: ServerException })
  async usuario(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<GetUsuarioResponseDto> {
    const usuario = await this.usuarioService.findOne({ id });
    if (!usuario) {
      throw new HttpException(undefined, HttpStatus.NOT_FOUND);
    }
    const roles = usuario.roles.map((rol) => rol.rolId);
    return new GetUsuarioResponseDto({ ...usuario, roles });
  }

  @Post()
  @Roles(Rol.ADMINISTRADOR)
  @ApiBearerAuth()
  @ApiCreatedResponse({ type: CreateUsuarioResponseDto })
  @ApiBadRequestResponse({ type: ServerException })
  @ApiInternalServerErrorResponse({ type: ServerException })
  async crearUsuario(
    @Body() usuarioDto: CreateUsuarioRequestDto,
  ): Promise<CreateUsuarioResponseDto> {
    const { nombre, username, password, roles } = usuarioDto;
    const { hashedPassword } = hashPassword(password);

    const usuario: Prisma.UsuarioCreateInput = {
      nombre,
      username,
      password: hashedPassword,
      roles: { create: roles.map((rolId) => ({ rolId })) },
    };

    return new CreateUsuarioResponseDto(
      await this.usuarioService.create(usuario),
    );
  }
}

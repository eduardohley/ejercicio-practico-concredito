import { ApiProperty } from '@nestjs/swagger';
import { Usuario } from '@prisma/client';
import { DtoString } from "../../core/class-validator/dto-string.decorator";

export class UsuarioDto implements Usuario {
  @ApiProperty()
  id: number;

  @DtoString(4, 50)
  nombre: string;

  @DtoString(4, 20)
  username: string;

  @DtoString(8, 32)
  password: string;
}
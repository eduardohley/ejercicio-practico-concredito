import { ApiProperty, OmitType } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { ArrayMinSize, IsEnum, IsNotEmpty } from 'class-validator';
import { ValidatorMessage } from '../../core/class-validator/validator-messages';
import { Rol } from '../../auth/enums/rol.enum';
import { UsuarioDto } from './usuario.dto';

export class CreateUsuarioRequestDto extends OmitType(UsuarioDto, ['id']) {
  @IsNotEmpty()
  @ArrayMinSize(1, { message: ValidatorMessage.arrayMinLength })
  @IsEnum(Rol, {
    message: ValidatorMessage.containsEnum,
    each: true,
  })
  @ApiProperty({ enum: Rol, isArray: true })
  roles: Rol[];
}

export class CreateUsuarioResponseDto extends OmitType(UsuarioDto, [
  'password',
]) {
  @Exclude()
  password: string;

  @ApiProperty({enum: Rol, isArray: true})
  roles: string[];

  constructor(partial: Partial<CreateUsuarioResponseDto>) {
    super();
    Object.assign(this, partial);
  }
}

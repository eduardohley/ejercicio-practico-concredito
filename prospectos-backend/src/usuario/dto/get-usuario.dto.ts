import { ApiProperty, OmitType } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { IsEnum } from 'class-validator';
import { Rol } from '../../auth/enums/rol.enum';
import { UsuarioDto } from './usuario.dto';

export class GetUsuarioResponseDto extends OmitType(UsuarioDto, ['password']) {
  @Exclude()
  password: string;

  @IsEnum(Rol, {
    each: true,
  })
  @ApiProperty({enum: Rol, isArray: true})
  roles: string[];

  constructor(partial: Partial<GetUsuarioResponseDto>) {
    super();
    Object.assign(this, partial);
  }
}

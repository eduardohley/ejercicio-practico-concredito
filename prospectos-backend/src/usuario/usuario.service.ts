import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { Prisma } from '@prisma/client';

@Injectable()
export class UsuarioService {
  constructor(private prismaService: PrismaService) {}

  async getRoles(usuarioId: number) {
    const result = await this.prismaService.usuario.findUnique({ where: {id: usuarioId}, select: {roles: true} });
    if (result) {
      return result.roles;
    }
    return [];
  }

  async findOne(where: Prisma.UsuarioWhereUniqueInput) {
    return await this.prismaService.usuario.findUnique({ where, include: {roles: true} });
  }

  async create(data: Prisma.UsuarioCreateInput) {
    return await this.prismaService.usuario.create({ data });
  }

  async delete(where: Prisma.UsuarioWhereUniqueInput) {
    return await this.prismaService.usuario.delete({ where });
  }
}

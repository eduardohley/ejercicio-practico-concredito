import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsuarioModule } from './usuario/usuario.module';
import { APP_FILTER, APP_GUARD } from '@nestjs/core';
import { AuthModule } from './auth/auth.module';
import { ProspectoModule } from './prospecto/prospecto.module';
import {
  HttpExceptionFilter,
  PrismaClientValidationErrorFilter,
  PrismaRequestErrorFilter,
} from './core/exceptions';
import { DocumentoProspectoModule } from './documento-prospecto/documento-prospecto.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { JwtAuthGuard } from './auth/guards/jwt-auth.guard';
import { RolesGuard } from './auth/guards/roles.guard';
import { ServeStaticModule } from '@nestjs/serve-static';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    EventEmitterModule.forRoot(),
    ServeStaticModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => [
        {
          rootPath: configService.get<string>("UPLOAD_PATH"),
        },
      ],
      inject: [ConfigService],
    }),
    AuthModule,
    UsuarioModule,
    ProspectoModule,
    DocumentoProspectoModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    { provide: APP_FILTER, useClass: PrismaClientValidationErrorFilter },
    {
      provide: APP_FILTER,
      useClass: PrismaRequestErrorFilter,
    },
    { provide: APP_FILTER, useClass: HttpExceptionFilter },
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },
  ],
})
export class AppModule {}

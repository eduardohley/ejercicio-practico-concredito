export const HttpErrorMessage = {
  NOT_FOUND: 'El recurso solicitado no existe',
  INTERNAL_ERROR: 'Error interno',
  UNAUTHORIZED: 'No autorizado o la sesión ha expirado',
  FORBIDDEN: 'No cuenta con los permisos necesarios para realizar esta operación',
  NOT_VALIDATED: 'Mensaje de error no validado',
};

export const PrismaErrorCode = {
  UNIQUE_CONSTRAINT: 'P2002',
  FOREIGN_KEY: 'P2003',
  NULL_CONSTRAINT: 'P2011',
  RECORD_DOES_NOT_EXIST: 'P2025',
};

export const PrismaErrorMessage = {
  UNIQUE_CONSTRAINT: 'Error de restricción único',
  FOREIGN_KEY: 'Error de llave foránea',
  NULL_CONSTRAINT: 'Error de restricción no nulo',
  RECORD_DOES_NOT_EXIST: 'El registro no existe en la base de datos',
  NOT_VALIDATED: 'Error de base de datos no validado',
  CLIENT_VALIDATION: 'Error de validación en el cliente de Prisma',
};

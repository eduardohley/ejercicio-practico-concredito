import { ApiProperty } from '@nestjs/swagger';

export class ServerException {
  @ApiProperty()
  statusCode: number;

  @ApiProperty({ type: String, isArray: true })
  message: string[];

  @ApiProperty()
  error?: string;
}

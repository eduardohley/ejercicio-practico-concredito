import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
} from '@nestjs/common';
import { Response } from 'express';
import { getHttpErrorMessage } from './error-messages';
import { ServerException } from './server-exception';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const status = exception.getStatus();
    const exceptionResponse = exception.getResponse();

    let messages: string[] = [];

    if (typeof exceptionResponse === 'object') {
      let message = (exceptionResponse as HttpException).message;
      message = getHttpErrorMessage(status, message);
      messages = Array.isArray(message) ? message : [message];
    }
    if (typeof exceptionResponse === 'string') {
      const message = exceptionResponse
        ? exceptionResponse
        : getHttpErrorMessage(status, exceptionResponse);
      messages = [message];
    }

    const serverException: ServerException = {
      statusCode: status,
      message: messages,
      error: exception.name,
    };

    response.status(status).json(serverException);
  }
}

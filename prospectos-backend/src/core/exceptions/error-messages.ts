import { HttpStatus } from '@nestjs/common';
import { HttpErrorMessage } from '../constants/http';
import { PrismaErrorCode, PrismaErrorMessage } from '../constants/prisma';

export function getHttpErrorMessage(code: number, originalMessage?: string): string {
  switch (code) {
    case HttpStatus.NOT_FOUND:
      return HttpErrorMessage.NOT_FOUND;
    case HttpStatus.INTERNAL_SERVER_ERROR:
      return HttpErrorMessage.INTERNAL_ERROR;
    case HttpStatus.UNAUTHORIZED:
      return HttpErrorMessage.UNAUTHORIZED;
    case HttpStatus.FORBIDDEN:
      return HttpErrorMessage.FORBIDDEN;
    default:
      return originalMessage || HttpErrorMessage.NOT_VALIDATED;
  }
}

export function getPrismaErrorMessage(code: string, meta: any): string {
  let { target } = meta;
  if (!target) target = '?';

  switch (code) {
    case PrismaErrorCode.UNIQUE_CONSTRAINT:
      return `${PrismaErrorMessage.UNIQUE_CONSTRAINT} <${target}>`;
    case PrismaErrorCode.FOREIGN_KEY:
      return `${PrismaErrorMessage.FOREIGN_KEY} <${target}>`;
    case PrismaErrorCode.NULL_CONSTRAINT:
      return `${PrismaErrorMessage.NULL_CONSTRAINT} <${target}>`;
    case PrismaErrorCode.RECORD_DOES_NOT_EXIST:
      return PrismaErrorMessage.RECORD_DOES_NOT_EXIST;
    default:
      return `${PrismaErrorMessage.NOT_VALIDATED} (${code})`;
  }
}

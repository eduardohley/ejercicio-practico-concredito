import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpStatus,
} from '@nestjs/common';
import {
  PrismaClientKnownRequestError,
  PrismaClientValidationError,
} from '@prisma/client/runtime';
import { Response } from 'express';
import { PrismaErrorMessage } from '../constants/prisma';
import { getPrismaErrorMessage } from './error-messages';
import { ServerException } from './server-exception';

@Catch(PrismaClientKnownRequestError)
export class PrismaRequestErrorFilter implements ExceptionFilter {
  catch(exception: PrismaClientKnownRequestError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    const { code, meta } = exception;

    const message = getPrismaErrorMessage(code, meta);
    const serverException: ServerException = {
      statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
      message: [message],
      error: exception.name,
    };

    response.status(HttpStatus.INTERNAL_SERVER_ERROR).send(serverException);
  }
}

@Catch(PrismaClientValidationError)
export class PrismaClientValidationErrorFilter implements ExceptionFilter {
  catch(exception: PrismaClientValidationError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    console.error(exception.message);

    const serverException: ServerException = {
      statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
      message: [PrismaErrorMessage.CLIENT_VALIDATION],
      error: exception.name,
    };

    response.status(HttpStatus.INTERNAL_SERVER_ERROR).send(serverException);
  }
}

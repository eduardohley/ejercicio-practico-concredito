import { genSaltSync, hashSync } from "bcrypt";
import { HashedPassword } from "./types/hashed-hassword";

export function hashPassword(password: string): HashedPassword {
  const salt = genSaltSync(10);
  const hashedPassword = hashSync(password, salt);

  return { salt, hashedPassword }
}
export type HashedPassword = {
  salt: string;
  hashedPassword: string;
}
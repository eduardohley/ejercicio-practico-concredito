import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsString, MinLength, MaxLength } from 'class-validator';
import { remmoveSpaces } from '../../utils/removeSpaces';
import { ValidatorMessage } from './validator-messages';

/**
 * Combina los decoradores @IsString @MinLength @MaxLength @ApiProperty
 * @param minLength
 * @param maxLength
 */
export function DtoString(minLength?: number, maxLength?: number, required: boolean = true) {
  const isStringFn = IsString({ message: ValidatorMessage.propertyTypeError });
  const minLengthFn = MinLength(minLength, { message: ValidatorMessage.minLength });
  const maxLengthFn = MaxLength(maxLength, { message: ValidatorMessage.maxLength });
  const apiPropertyFn = ApiProperty({ minLength, maxLength, required });
  const transformFn = Transform(({value}) => remmoveSpaces((value || "").toString().trim()));

  return function (target: any, key: string) {
    isStringFn(target, key);
    minLengthFn(target, key);
    maxLengthFn(target, key);
    transformFn(target, key);
    apiPropertyFn(target, key);
  };
}

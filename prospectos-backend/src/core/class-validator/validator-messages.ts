import { ValidationArguments } from 'class-validator';

namespace ValidatorMessage {
  export function propertyTypeError(
    args: Partial<ValidationArguments>,
  ): string {
    return `No se permite el tipo ${typeof args.value} en la propiedad ${
      args.property
    }`;
  }

  export function minLength(
    args: Partial<Partial<ValidationArguments>>,
  ): string {
    const [constraint] = args.constraints;
    const plural = constraint > 1 ? 'es' : '';
    return `${args.property} debe contener al menos ${args.constraints} caracter${plural}`;
  }

  export function maxLength(args: Partial<ValidationArguments>): string {
    const [constraint] = args.constraints;
    const plural = constraint > 1 ? 'es' : '';
    return `${args.property} debe contener un máximo de ${args.constraints} caracter${plural}`;
  }

  export function arrayMinLength(args: Partial<ValidationArguments>): string {
    const [constraint] = args.constraints;
    const plural = constraint > 1 ? 's' : '';
    return `${args.property} debe contener al menos ${args.constraints} elemento${plural}`;
  }

  export function isEnum(args: Partial<ValidationArguments>) {
    const [constraint] = args.constraints;
    const enumValues = Object.keys(constraint).map((e) => `'${e}'`);
    return `${
      args.property
    } debe contener uno de los siguientes elementos: [${enumValues.join(', ')}]`;
  }

  export function containsEnum(args: Partial<ValidationArguments>) {
    const [constraint] = args.constraints;
    const enumValues = Object.keys(constraint).map((e) => `'${e}'`);
    return `${
      args.property
    } debe contener al menos 1 de los siguientes elementos: ${enumValues.join(
      ', ',
    )}`;
  }

  export function empty(args: Partial<ValidationArguments>) {
    return `${args.property} no debe estar vacio`;
  }
}

export { ValidatorMessage };

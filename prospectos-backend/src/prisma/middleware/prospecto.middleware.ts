import { HttpException, HttpStatus } from '@nestjs/common';
import { Prisma } from '@prisma/client';

export async function prospectoMiddleware(
  params: Prisma.MiddlewareParams,
  next: (params: Prisma.MiddlewareParams) => Promise<any>,
) {
  if (params.model === 'Prospecto') {
    if (params.action === 'findMany' || params.action === 'findUnique') {
      let resultado: any = await next(params);
      if (!resultado) {
        throw new HttpException('El prospecto no existe', HttpStatus.CONFLICT);
      }
      
      if (resultado) {
        if (Array.isArray(resultado)) {
          resultado = resultado.map((prospecto: any) => ({
            ...prospecto,
            documentos: prospecto.documentos.map((doc: any) => ({
              ...doc,
              archivo: `/documento-prospecto/${prospecto.id}/${doc.archivo}`,
            })),
          }));
        } else {
          resultado.documentos = resultado.documentos.map((doc: any) => ({
            ...doc,
            archivo: `/documento-prospecto/${resultado.id}/${doc.archivo}`,
          }));
        }
      }

      return resultado;
    }
  }

  return await next(params);
}

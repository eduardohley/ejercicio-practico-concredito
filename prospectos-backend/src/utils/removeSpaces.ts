export function remmoveSpaces(text: string): string {
  return '' + text.replace(/ +(?= )/g, '');
}

export function removeNonDigits(value: string): string {
  if (!value) {
    return "0";
  }

  return value.replace(/\D/g,'');
}
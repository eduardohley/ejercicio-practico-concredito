import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ProspectoService } from './prospecto.service';
import {
  CreateProspectoRequestDto,
  CreateProspectoResponseDto,
  UpdateProspectoRequestDto,
  GetProspectoResponseDto,
  DeleteProspectoResponseDto,
  UpdateProspectoResponseDto,
} from './dto';
import { ServerException } from '../core/exceptions';
import { Roles } from '../auth/decorators/roles.decorator';
import { Rol } from '../auth/enums/rol.enum';

@ApiTags('Prospecto')
@Controller('prospecto')
export class ProspectoController {
  constructor(private prospectoService: ProspectoService) {}

  @Get()
  @Roles(Rol.PROMOTOR_PROSPECTOS, Rol.EVALUADOR_PROSPECTOS)
  @ApiBearerAuth()
  @ApiOkResponse({ type: GetProspectoResponseDto })
  @ApiInternalServerErrorResponse({ type: ServerException })
  async getProspectos(): Promise<GetProspectoResponseDto[]> {
    const prospectos = await this.prospectoService.findMany({
      include: { documentos: true },
    });
    return prospectos as GetProspectoResponseDto[];
  }

  @Get(':id')
  @Roles(Rol.PROMOTOR_PROSPECTOS, Rol.EVALUADOR_PROSPECTOS)
  @ApiBearerAuth()
  @ApiOkResponse({ type: GetProspectoResponseDto })
  async getProspecto(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<GetProspectoResponseDto> {
    return await this.prospectoService.findOne({ id });
  }

  @Post()
  @Roles(Rol.PROMOTOR_PROSPECTOS)
  @ApiBearerAuth()
  @ApiCreatedResponse({ type: CreateProspectoResponseDto })
  @ApiInternalServerErrorResponse({ type: ServerException })
  async crearProspecto(@Body() prospectoDto: CreateProspectoRequestDto) {
    return await this.prospectoService.create(prospectoDto);
  }

  @Put(':id')
  @Roles(Rol.PROMOTOR_PROSPECTOS, Rol.EVALUADOR_PROSPECTOS)
  @ApiBearerAuth()
  @ApiOkResponse({ type: UpdateProspectoResponseDto })
  @ApiInternalServerErrorResponse({ type: ServerException })
  async updateProspecto(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateProspectoDto: UpdateProspectoRequestDto,
  ): Promise<UpdateProspectoResponseDto> {
    return await this.prospectoService.update({ id }, updateProspectoDto);
  }

  @Delete(':id')
  @Roles(Rol.PROMOTOR_PROSPECTOS)
  @ApiBearerAuth()
  @ApiOkResponse({ type: DeleteProspectoResponseDto })
  @ApiInternalServerErrorResponse({ type: ServerException })
  async deleteProspecto(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<DeleteProspectoResponseDto> {
    return await this.prospectoService.delete({ id });
  }
}

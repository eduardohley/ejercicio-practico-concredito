import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class ProspectoService {
  constructor(private prismaService: PrismaService) {}

  async findMany(args: Prisma.ProspectoFindManyArgs) {
    return this.prismaService.prospecto.findMany(args);
  }

  async findOne(where: Prisma.ProspectoWhereUniqueInput) {
    return this.prismaService.prospecto.findUnique({
      where,
      include: { documentos: true },
    });
  }

  async create(data: Prisma.ProspectoCreateInput) {
    return this.prismaService.prospecto.create({ data });
  }

  async update(
    where: Prisma.ProspectoWhereUniqueInput,
    data: Prisma.ProspectoUpdateInput,
  ) {
    return this.prismaService.prospecto.update({ where, data });
  }

  async delete(where: Prisma.ProspectoWhereUniqueInput) {
    return this.prismaService.prospecto.delete({ where });
  }
}

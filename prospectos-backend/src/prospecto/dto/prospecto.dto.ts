import { ApiProperty } from '@nestjs/swagger';
import { Prospecto, EstadoProspecto } from '@prisma/client';
import { IsEnum, IsOptional } from 'class-validator';
import { DtoString } from '../../core/class-validator/dto-string.decorator';
import { ValidatorMessage } from '../../core/class-validator/validator-messages';

export class ProspectoDto implements Prospecto {
  @ApiProperty()
  id: number;

  @DtoString(1, 50)
  nombre: string;

  @DtoString(1, 50)
  primerApellido: string;

  @IsOptional()
  @DtoString(0, 50, false)
  segundoApellido: string;

  @DtoString(6, 15)
  telefono: string;

  @DtoString(12, 13)
  rfc: string;

  @DtoString(1, 50)
  calle: string;

  @DtoString(1, 10)
  numero: string;

  @DtoString(1, 50)
  colonia: string;

  @DtoString(1, 20)
  codigoPostal: string;

  @IsOptional()
  @IsEnum(EstadoProspecto, {message: ValidatorMessage.containsEnum})
  @ApiProperty({ enum: EstadoProspecto, required: false })
  estado: EstadoProspecto;

  @IsOptional()
  @DtoString(0, 300, false)
  observaciones: string;
}

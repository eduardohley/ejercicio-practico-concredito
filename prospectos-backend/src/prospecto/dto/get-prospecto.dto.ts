import { ApiProperty } from '@nestjs/swagger';
import { ProspectoDto } from './prospecto.dto';
import { DocumentoProspectoDto } from '../../documento-prospecto/dto/documento-prospecto.dto';

export class GetProspectoResponseDto extends ProspectoDto {
  @ApiProperty({ type: DocumentoProspectoDto, isArray: true })
  documentos: any[];
}

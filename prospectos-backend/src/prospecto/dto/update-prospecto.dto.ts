import { OmitType } from '@nestjs/swagger';
import { ProspectoDto } from './prospecto.dto';

export class UpdateProspectoRequestDto extends OmitType(ProspectoDto, ['id']) {}

export class UpdateProspectoResponseDto extends ProspectoDto {}

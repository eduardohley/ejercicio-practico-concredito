import { ApiProperty, OmitType } from '@nestjs/swagger';
import { DocumentoProspectoDto } from '../../documento-prospecto/dto/documento-prospecto.dto';
import { ProspectoDto } from './prospecto.dto';

export class CreateProspectoRequestDto extends OmitType(ProspectoDto, ['id']) {}

export class CreateProspectoResponseDto extends ProspectoDto {
  @ApiProperty({ type: DocumentoProspectoDto, isArray: true })
  documentos: DocumentoProspectoDto[];
}

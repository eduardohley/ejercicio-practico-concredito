import { Module } from '@nestjs/common';
import { PrismaModule } from '../prisma/prisma.module';
import { ProspectoController } from './prospecto.controller';
import { ProspectoService } from './prospecto.service';

@Module({
  imports: [PrismaModule],
  controllers: [ProspectoController],
  providers: [ProspectoService],
  exports: [ProspectoService],
})
export class ProspectoModule {}

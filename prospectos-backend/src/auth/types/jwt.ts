export type JWTPayload = {
  nombre: string;
  username: string;
  sub: number;
  roles?: string[];
  iat?: number;
  exp?: number;
}

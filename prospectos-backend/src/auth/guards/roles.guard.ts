import { Injectable, CanActivate, ExecutionContext, ForbiddenException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UsuarioService } from '../../usuario/usuario.service';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector, private usuarioService: UsuarioService) {}

  async canActivate(context: ExecutionContext) {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    console.info("Controller roles:", roles);
    
    
    if (!roles) {
      return true;
    }
    
    const request = context.switchToHttp().getRequest();
    
    const { user } = request;
    
    const usuario = await this.usuarioService.findOne({id: user.sub})
    if (usuario) {
      const usuarioRoles = usuario.roles.map(rol => rol.rolId);
      console.info("Usuario roles:", usuarioRoles);
      const allow = roles.some((r) => usuarioRoles.includes(r));
      
      return allow;
    }
    
    throw new ForbiddenException();
  }
}
import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { ValidatorMessage } from '../../core/class-validator/validator-messages';

export class PostLoginRequestDto {
  @IsString({ message: ValidatorMessage.propertyTypeError })
  @ApiProperty()
  username: string;

  @IsString({ message: ValidatorMessage.propertyTypeError })
  @ApiProperty()
  password: string;
}

export class PostLoginResponseDto {
  @ApiProperty()
  accessToken: string;
}

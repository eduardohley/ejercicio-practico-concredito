import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { ValidatorMessage } from '../../core/class-validator/validator-messages';

export class PostRenewTokenDto {
  @IsString({ message: ValidatorMessage.propertyTypeError })
  @ApiProperty()
  accessToken: string;
}

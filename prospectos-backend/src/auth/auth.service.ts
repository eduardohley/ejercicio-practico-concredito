import { Injectable } from '@nestjs/common';
import { compareSync } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { JWTPayload } from './types/jwt';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private usuarioService: UsuarioService,
  ) {}

  async validarCredenciales(
    username: string,
    password: string,
  ): Promise<number | false> {
    const usuario = await this.usuarioService.findOne({ username });

    if (usuario && compareSync(password, usuario.password)) {
      return usuario.id;
    }

    return false;
  }

  async generarToken(nombre: string, username: string, sub: number, roles: string[]) {
    const payload: JWTPayload = { nombre, username, sub, roles };
    return {
      accessToken: this.jwtService.sign(payload),
      username,
      sub,
      roles,
    };
  }
}

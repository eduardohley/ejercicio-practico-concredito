import {
  Body,
  Controller,
  HttpCode,
  HttpException,
  HttpStatus,
  Post,
} from '@nestjs/common';
import {
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { AuthService } from './auth.service';
import {
  PostLoginRequestDto,
  PostLoginResponseDto,
} from './dto/post-login.dto';
import { UnauthorizedResponse } from './dto/unauthorized.dto';
import { Public } from './decorators/public.decorator';
import { UsuarioService } from '../usuario/usuario.service';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private usuarioService: UsuarioService,
  ) {}

  @Public()
  @Post('login')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ type: PostLoginResponseDto })
  @ApiUnauthorizedResponse({ type: UnauthorizedResponse })
  async login(@Body() loginDto: PostLoginRequestDto) {
    const { username, password } = loginDto;

    const sub = await this.authService.validarCredenciales(username, password);

    if (sub) {
      const { nombre, username, roles } = await this.usuarioService.findOne({
        id: sub,
      });
      const rolesIds = roles.map((rol) => rol.rolId);
      return await this.authService.generarToken(
        nombre,
        username,
        sub,
        rolesIds,
      );
    }

    throw new HttpException("Credenciales inválidas", HttpStatus.BAD_REQUEST);
  }
}

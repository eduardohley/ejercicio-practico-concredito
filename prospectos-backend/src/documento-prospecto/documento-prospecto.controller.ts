import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  ParseArrayPipe,
  ParseIntPipe,
  Post,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
} from '@nestjs/swagger';
import { Prisma } from '@prisma/client';
import { Roles } from '../auth/decorators/roles.decorator';
import { Rol } from '../auth/enums/rol.enum';
import { ValidatorMessage } from '../core/class-validator/validator-messages';
import { ServerException } from '../core/exceptions';
import { ProspectoService } from '../prospecto/prospecto.service';
import { DocumentoProspectoService } from './documento-prospecto.service';
import {
  CreateDocumentoProspectoRequestDto,
  CreateDocumentoProspectoResponseDto,
} from './dto';

@Controller('documento-prospecto')
export class DocumentoProspectoController {
  constructor(
    private documentoProspectoService: DocumentoProspectoService,
    private prospectoService: ProspectoService,
  ) {}

  @Post()
  @Roles(Rol.PROMOTOR_PROSPECTOS)
  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: CreateDocumentoProspectoRequestDto,
  })
  @UseInterceptors(FilesInterceptor('documentos'))
  @ApiCreatedResponse({ type: CreateDocumentoProspectoResponseDto })
  @ApiInternalServerErrorResponse({ type: ServerException })
  async createDocumentos(
    @Body('prospectoId', ParseIntPipe) prospectoId: number,
    @Body('nombres', ParseArrayPipe) nombres: string[],
    @UploadedFiles() documentos: Array<Express.Multer.File>,
  ) {
    if (!documentos || documentos.length === 0) {
      throw new HttpException(
        ValidatorMessage.arrayMinLength({
          property: 'documentos',
          constraints: [1],
        }),
        HttpStatus.BAD_REQUEST,
      );
    }

    if (!prospectoId) {
      throw new HttpException('Indique el id prospecto', HttpStatus.CONFLICT);
    }
    const prospecto = await this.prospectoService.findOne({ id: prospectoId });
    if (!prospecto) {
      throw new HttpException('El prospecto no existe', HttpStatus.CONFLICT);
    }

    if (nombres.length < documentos.length) {
      nombres = documentos.map(
        (doc, i) =>
          nombres[i] ||
          doc.originalname.substring(0, doc.originalname.lastIndexOf('.')),
      );
    }

    const documentosProspectos: Prisma.DocumentoProspectoCreateManyArgs = {
      data: documentos.map((doc, i) => ({
        nombre: nombres[i] || doc.originalname.substring(0, doc.originalname.lastIndexOf('.')),
        archivo: doc.filename,
        prospectoId,
      })),
    };

    return await this.documentoProspectoService.createMany(
      documentosProspectos,
    );
  }
}

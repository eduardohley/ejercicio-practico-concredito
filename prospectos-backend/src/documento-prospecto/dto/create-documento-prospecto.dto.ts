import { ApiProperty, OmitType } from '@nestjs/swagger';
import { Prisma } from '@prisma/client';
import { DocumentoProspectoDto } from './documento-prospecto.dto';

export class CreateDocumentoProspectoRequestDto extends OmitType(
  DocumentoProspectoDto,
  ['id', 'nombre', 'archivo'],
) {
  prospectoId: number;
  @ApiProperty({ type: String, isArray: true })
  nombres: [];
  
  @ApiProperty({
    type: 'array',
    items: { type: 'string', format: 'binary' },
    minLength: 1,
  })
  documentos: any[];
}

export class CreateDocumentoProspectoResponseDto
  implements Prisma.BatchPayload
{
  @ApiProperty()
  count: number;
}

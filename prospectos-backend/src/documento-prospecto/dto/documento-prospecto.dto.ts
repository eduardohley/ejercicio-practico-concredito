import { ApiProperty } from "@nestjs/swagger";
import { DocumentoProspecto } from "@prisma/client";
import { Transform } from "class-transformer";
import { IsNotEmpty } from "class-validator";
import { DtoString } from "../../core/class-validator/dto-string.decorator";

export class DocumentoProspectoDto implements DocumentoProspecto {
  @ApiProperty()
  id: number;

  @DtoString()
  @ApiProperty()
  nombre: string;

  @DtoString()
  @ApiProperty()
  archivo: string;

  @IsNotEmpty()
  @Transform(({value}) => Number(value.toString().replaceAll("[^\\d.]", "")))
  @ApiProperty()
  prospectoId: number;
}
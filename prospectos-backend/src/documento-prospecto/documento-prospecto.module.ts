import { HttpException, HttpStatus, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MulterModule } from '@nestjs/platform-express';
import { existsSync, mkdirSync } from 'fs';
import { diskStorage } from 'multer';
import { v4 as uuid } from 'uuid';
import { PrismaModule } from '../prisma/prisma.module';
import { ProspectoModule } from '../prospecto/prospecto.module';
import { ProspectoService } from '../prospecto/prospecto.service';
import { removeNonDigits } from '../utils/removeNonDigits';
import { DocumentoProspectoController } from './documento-prospecto.controller';
import { DocumentoProspectoService } from './documento-prospecto.service';

@Module({
  imports: [
    PrismaModule,
    ProspectoModule,
    MulterModule.registerAsync({
      imports: [ConfigModule, ProspectoModule],
      useFactory: async (
        configService: ConfigService,
        prospectoService: ProspectoService,
      ) => {
        const storage = diskStorage({
          destination: async (req, file, cb) => {
            const prospectoId = removeNonDigits(req.body.prospectoId);

            let error: any;

            const prospecto = await prospectoService.findOne({
              id: Number(prospectoId),
            });

            if (!prospecto) {
              error = new HttpException(
                'El prospecto no existe',
                HttpStatus.CONFLICT,
              );
              return cb(error, null);
            }

            const uploadPath = configService.get<string>('UPLOAD_PATH');
            const destPath = uploadPath + '/documento-prospecto/' + prospectoId;

            if (!existsSync(uploadPath)) {
              error = new HttpException(
                `La ruta ${uploadPath} para los archivos no existe`,
                HttpStatus.INTERNAL_SERVER_ERROR,
              );
            } else {
              if (!existsSync(destPath)) {
                mkdirSync(destPath, { recursive: true });
              }
            }

            cb(error, destPath);
          },
          filename: (req, file, cb) => {
            const filename = uuid() + '-' + file.originalname;
            cb(null, filename);
          },
        });
        return {
          limits: { fileSize: configService.get<number>('UPLOAD_MAX_SIZE') },
          fileFilter: (req, file, cb) => cb(null, true),
          storage,
        };
      },
      inject: [ConfigService, ProspectoService],
    }),
  ],
  controllers: [DocumentoProspectoController],
  providers: [DocumentoProspectoService],
  exports: [DocumentoProspectoService],
})
export class DocumentoProspectoModule {}

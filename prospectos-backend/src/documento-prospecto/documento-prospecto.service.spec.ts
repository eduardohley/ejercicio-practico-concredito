import { Test, TestingModule } from '@nestjs/testing';
import { DocumentoProspectoService } from './documento-prospecto.service';

describe('DocumentoProspectoService', () => {
  let service: DocumentoProspectoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DocumentoProspectoService],
    }).compile();

    service = module.get<DocumentoProspectoService>(DocumentoProspectoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import { Test, TestingModule } from '@nestjs/testing';
import { DocumentoProspectoController } from './documento-prospecto.controller';

describe('DocumentoProspectoController', () => {
  let controller: DocumentoProspectoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DocumentoProspectoController],
    }).compile();

    controller = module.get<DocumentoProspectoController>(DocumentoProspectoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class DocumentoProspectoService {
  constructor(private prismaService: PrismaService) {}

  deleteMany(where: Prisma.DocumentoProspectoWhereInput) {
    return this.prismaService.documentoProspecto.deleteMany({ where });
  }

  createMany(args: Prisma.DocumentoProspectoCreateManyArgs) {
    return this.prismaService.documentoProspecto.createMany(args);
  }
}

import { MessageParams } from "yup/lib/types";

namespace YupMessages {
  export function required({ label }: Partial<MessageParams>) {
    return `El campo ${label || "<sin etiqueta>"} es requerido`;
  }

  export function min({ label, min }: { min: number } & Partial<MessageParams>) {
    return `El campo ${label || "<sin etiqueta>"}  debe tener un mínimo de ${min} caracteres`;
  }

  export function max({ label, max }: { max: number } & Partial<MessageParams>) {
    return `El campo ${label || "<sin etiqueta>"}  debe tener un máximo de ${max} caracteres`;
  }

  export function typeError({ label, type }: Partial<MessageParams>) {
    let tipo = "texto";
    if (type === "number") tipo = "números";

    return `El campo ${label} debe contener solo ${tipo}`;
  }
}

export { YupMessages };

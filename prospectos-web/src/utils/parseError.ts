import { ServerException } from "../api/exception/dto/ServerException";

export function parseError(error: any): ServerException {
  if (error.message === "Network Error") {
    return {message: ["Error de conexión"], statusCode: 0}
  }
  const serverException: ServerException = error.response.data;
  return serverException;
}
import React from "react";

export function childrenWithProps(children: React.ReactNode) {
  return React.Children.map(children, (child) => {
    if (React.isValidElement(child)) {
      return React.cloneElement(child);
    }
    return child;
  });
}

import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

type BasicDialogProps = {
  title: string;
  text: string;
  confirmText?: string;
  onConfirm: () => void
};

export function BasicDialog({ title, text, confirmText, onConfirm = () => {} }: BasicDialogProps) {
  return (
    <Dialog open={true} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
      <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">{text}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="success" onClick={onConfirm} autoFocus>
          {confirmText || "Aceptar"}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

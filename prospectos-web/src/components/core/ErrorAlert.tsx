import { Alert } from "@mui/material";
import { ServerException } from "../../api/exception/dto/ServerException";

type ErrorAlertProps = {
  error: ServerException;
};

export const ErrorAlert = ({ error }: ErrorAlertProps) => {
  if (Array.isArray(error.message)) {
    return (
      <Alert variant="outlined" severity="error">
        {error.message.map((msg, i) => (
          <div key={i}>{msg}</div>
        ))}
      </Alert>
    );
  }

  if (typeof error.message === "string") {
    return (
      <Alert variant="outlined" severity="error">
        <div>{error.message}</div>
      </Alert>
    );
  }

  return (
    <Alert variant="outlined" severity="error">
      <div>Error desconocido</div>
    </Alert>
  );
};

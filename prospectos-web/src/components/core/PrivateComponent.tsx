import React from "react";
import { Route, Redirect } from "react-router-dom";
import type { RouteProps } from "react-router-dom";
import { useAuth } from "../../hooks/useAuth";
import { Rol } from "../../api/auth/rol.enum";

interface PrivateComponentParams extends RouteProps {
  children: React.ReactNode;
  roles?: Rol[];
}

export default function PrivateComponent({children, roles, ...props}: PrivateComponentParams) {
  const [usuario] = useAuth();

  return (
    <Route
      {...props}
      render={
        (props) => {
          if (!usuario) {
            return (
              <Redirect
                to={{
                  pathname: "/login",
                  state: { from: props.location },
                }}
              />
            );
          }

          if (roles && roles.length > 0) {
            const allow = roles.some((r) => usuario.roles?.includes(r));
            if (allow) {
              const childrenWithProps = React.Children.map(children, child => {
                if (React.isValidElement(child)) {
                  return React.cloneElement(child);
                }
                return child;
              });

              return childrenWithProps
            }

            return undefined;
          }
          
          const childrenWithProps = React.Children.map(children, child => {
            if (React.isValidElement(child)) {
              return React.cloneElement(child);
            }
            return child;
          });

          return childrenWithProps
        }
      }
    />
  );
}

import { Route, Redirect, RouteComponentProps } from "react-router-dom";
import type { RouteProps } from "react-router-dom";
import { useAuth } from "../../hooks/useAuth";
import { Rol } from "../../api/auth/rol.enum";
import React from "react";

interface PrivateRouteParams extends RouteProps {
  roles?: Rol[];
  component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
}

export default function PrivateRoute({ component: Component, roles, ...rest }: PrivateRouteParams) {
  const [usuario] = useAuth();

  return (
    <Route
      {...rest}
      render={
        (props) => {
          if (!usuario) {
            return (
              <Redirect
                to={{
                  pathname: "/login",
                  state: { from: props.location },
                }}
              />
            );
          }

          if (roles && roles.length > 0) {
            const allow = roles.some((r) => usuario.roles?.includes(r));

            if (allow) {
              return <Component {...props} />;
            }

            return <h1>Acceso denegado</h1>;
          }
          
          return <Component {...props} />;
        }
      }
    />
  );
}

import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

type ConfirmDialogProps = {
  title: string;
  text: string;
  onConfirm: () => void;
  onCancel: () => void;
  confirmText?: string;
  cancelText?: string;
};

export function ConfirmDialog({ title, text, onConfirm, onCancel, cancelText, confirmText }: ConfirmDialogProps) {
  return (
    <Dialog  open={true} onClose={onCancel} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
      <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">{text}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="error" onClick={onCancel}>{cancelText || "Cancelar"}</Button>
        <Button color="success" onClick={onConfirm} autoFocus>
          {confirmText || "Aceptar"}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

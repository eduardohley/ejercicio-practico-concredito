import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Logo from "../../assets/logos/concredito-logo.png";
import { LoginForm } from "./LoginForm";

export default function Login() {
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Box
          component="img"
          sx={{
            width: 400,
          }}
          alt="concredito-logo"
          src={Logo}
        />
        <Box component="div" sx={{ mt: 1 }}>
          <LoginForm />
        </Box>
      </Box>
    </Container>
  );
}

import { useFormik } from "formik";
import { TextField } from "@mui/material";
import { Auth } from "../../api/auth/login";
import { ServerException } from "../../api/exception/dto/ServerException";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { loginValidationSchema } from "./loginValidationSchema";
import { useAuth } from "../../hooks/useAuth";
import { ErrorAlert } from "../core/ErrorAlert";
import { parseError } from "../../utils/parseError";
import { LoadingButton } from "@mui/lab";
import LoginIcon from '@mui/icons-material/Login';

export const LoginForm = () => {
  const history = useHistory();
  const [usuario] = useAuth();
  const [error, setError] = useState<ServerException>();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (usuario) {
      history.push("/");
    }
  }, [usuario, history]);

  async function handleLogin({ username, password }: { username: string; password: string }) {
    setError(undefined);
    setLoading(true);
    try {
      const response = await Auth.login(username, password);
      const {
        status,
        data: { accessToken },
      } = response;

      if (status === 200 && accessToken) {
        Auth.setToken(accessToken);
        history.push("/");
      }
    } catch (error: any) {
      setError(parseError(error));
    } finally {
      setLoading(false);
    }
  }

  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    validationSchema: loginValidationSchema,
    onSubmit: handleLogin,
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <TextField
        margin="normal"
        required
        fullWidth
        id="username"
        label="Nombre de usuario"
        name="username"
        autoFocus
        value={formik.values.username}
        onChange={formik.handleChange}
        error={formik.touched.username && Boolean(formik.errors.username)}
        helperText={formik.touched.username && formik.errors.username}
      />
      <TextField
        margin="normal"
        required
        fullWidth
        name="password"
        label="Contraseña"
        type="password"
        id="password"
        value={formik.values.password}
        onChange={formik.handleChange}
        error={formik.touched.password && Boolean(formik.errors.password)}
        helperText={formik.touched.password && formik.errors.password}
      />
      <LoadingButton type="submit" loading={loading} startIcon={<LoginIcon />} fullWidth loadingPosition="start" variant="contained" sx={{mt: 3, mb: 2}}>
        Iniciar sesión
      </LoadingButton>
      {error && <ErrorAlert error={error} />}
    </form>
  );
};

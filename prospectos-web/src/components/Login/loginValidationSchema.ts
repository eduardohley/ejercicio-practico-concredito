import * as yup from "yup";

export const loginValidationSchema = yup.object({
  username: yup.string().required("Nombre de usuario requerido"),
  password: yup.string().required("Contraseña requerida"),
});
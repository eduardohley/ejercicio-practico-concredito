import React from "react";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import PeopleIcon from "@mui/icons-material/People";
import HomeIcon from "@mui/icons-material/Home";
import { Link } from "react-router-dom";
import { ListItem } from "@mui/material";
import PrivateComponent from "../core/PrivateComponent";
import { Rol } from "../../api/auth/rol.enum";

export const DrawerListItems = () => (
  <React.Fragment>
    <ListItem button component={Link} to="/">
      <ListItemIcon>
        <HomeIcon />
      </ListItemIcon>
      <ListItemText primary="Inicio" />
    </ListItem>
    <PrivateComponent roles={[Rol.PROMOTOR_PROSPECTOS, Rol.EVALUADOR_PROSPECTOS]}>
      <ListItem button component={Link} to="/prospectos">
        <ListItemIcon>
          <PeopleIcon />
        </ListItemIcon>
        <ListItemText primary="Prospectos" />
      </ListItem>
    </PrivateComponent>
  </React.Fragment>
);

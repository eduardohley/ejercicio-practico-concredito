import React, { useEffect, useState } from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import { Avatar, Button, Chip, Container, Divider, IconButton, List, ListItem, ListItemAvatar, ListItemText, Paper, Stack } from "@mui/material";
import { ServerException } from "../../../api/exception/dto/ServerException";
import { Box } from "@mui/system";
import { ConfirmDialog } from "../../core/ConfirmDialog";
import { useHistory, useParams } from "react-router-dom";
import { Prospecto } from "../../../api/prospecto/prospecto";
import AttachFileIcon from "@mui/icons-material/AttachFile";
import DoNotDisturbIcon from "@mui/icons-material/DoNotDisturb";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import OpenInNewIcon from "@mui/icons-material/OpenInNew";
import { GetProspectoResponseDto } from "../../../api/prospecto/dto/get-prospecto.dto";
import { ConCreditoColor } from "../../../theme/colors";
import { EstadoProspecto } from "../../../api/prospecto/dto/estado-prospecto.enum";
import { baseURL } from "../../../config/server";
import { RechazarDialog } from "./RechazarDialog";
import { ErrorAlert } from "../../core/ErrorAlert";
import { parseError } from "../../../utils/parseError";

const initialProspecto: GetProspectoResponseDto = {
  id: 0,
  nombre: "",
  primerApellido: "",
  segundoApellido: "",
  telefono: "",
  rfc: "",
  calle: "",
  numero: "",
  colonia: "",
  codigoPostal: "",
  documentos: [],
};

export function EvaluarProspecto() {
  const { prospectoId } = useParams<{ prospectoId: string }>();
  const history = useHistory();
  const [prospecto, setProspecto] = useState<GetProspectoResponseDto>(initialProspecto);
  const [error, setError] = useState<ServerException>();
  const [showAutorizarDialog, setShowAutorizarDialog] = useState(false);
  const [showRechazarDialog, setShowRechazarDialog] = useState(false);

  function handleExit() {
    history.push("/prospectos");
  }

  const handleUpdate = async (estado: EstadoProspecto, observaciones: string) => {
    setError(undefined);
    try {
      await Prospecto.update(Number(prospectoId), { ...prospecto, estado, observaciones });
      loadProspecto()
    } catch (error: any) {
      setError(parseError(error));
    } finally {
      setShowAutorizarDialog(false);
      setShowRechazarDialog(false);
    }
  };

  async function handleAutorizar() {
    await handleUpdate(EstadoProspecto.Autorizado, "");
  }

  async function handleRechazar(observaciones: string) {
    await handleUpdate(EstadoProspecto.Rechazado, observaciones);
  }

  async function loadProspecto() {
    try {
      const prospecto = await Prospecto.findById(Number(prospectoId));
      if (prospecto) {
        setProspecto(prospecto);
      } else {
        setProspecto(initialProspecto);
      }
    } catch (error: any) {
      setError(parseError(error));
    }
  }

  useEffect(() => {
    loadProspecto();
  }, []);

  return (
    <React.Fragment>
      <Typography component="h5" variant="h5" align="center">
        Evaluar Prospecto
      </Typography>
      <Container component="main" maxWidth="sm" sx={{ mb: 4 }}>
        <Paper variant="outlined" sx={{ my: { xs: 3 }, p: { xs: 2 } }}>
          <Grid container spacing={3} rowSpacing={2}>
            {error && (
              <Grid item xs={12}>
                <ErrorAlert error={error} />
              </Grid>
            )}
            <Grid item xs={6}>
              <Typography variant="h6" gutterBottom>
                Datos personales
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Stack direction="row" spacing={5}>
                {prospecto.estado === EstadoProspecto.Autorizado && <Chip label="Autorizado" color="success" icon={<CheckCircleOutlineIcon />} />}
                {prospecto.estado !== EstadoProspecto.Autorizado && <Chip label="Autorizar" onClick={() => setShowAutorizarDialog(true)} color="default" icon={<CheckCircleOutlineIcon />} />}
                {prospecto.estado === EstadoProspecto.Rechazado && <Chip label="Rechazado" color="error" icon={<DoNotDisturbIcon />} />}
                {prospecto.estado !== EstadoProspecto.Rechazado && <Chip label="Rechazar" onClick={() => setShowRechazarDialog(true)} color="default" icon={<DoNotDisturbIcon />} />}
              </Stack>
            </Grid>
            <Grid item xs={12}>
              <TextField
                label="Nombre"
                fullWidth
                variant="standard"
                value={prospecto.nombre}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="Primer apellido"
                fullWidth
                variant="standard"
                value={prospecto.primerApellido}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="Segundo apellido"
                fullWidth
                variant="standard"
                value={prospecto.segundoApellido}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="Teléfono"
                fullWidth
                autoComplete="shipping address-line2"
                variant="standard"
                value={prospecto.telefono}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="RFC"
                fullWidth
                variant="standard"
                value={prospecto.rfc}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={12} sx={{ mt: 2 }}>
              <Typography variant="h6" gutterBottom>
                Dirección
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="Calle"
                fullWidth
                variant="standard"
                value={prospecto.calle}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="Número"
                fullWidth
                variant="standard"
                value={prospecto.numero}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="Colonia"
                fullWidth
                variant="standard"
                value={prospecto.colonia}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="Código postal"
                fullWidth
                variant="standard"
                value={prospecto.codigoPostal}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={12} sx={{ mt: 2 }}>
              <Typography variant="h6" gutterBottom>
                Documentos
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <List dense={true}>
                {prospecto.documentos.map((doc, i) => (
                  <React.Fragment key={i}>
                    <ListItem
                      secondaryAction={
                        <IconButton edge="end" aria-label="delete" target="_blank" href={baseURL + doc.archivo}>
                          <OpenInNewIcon />
                        </IconButton>
                      }
                    >
                      <ListItemAvatar>
                        <Avatar sx={{ bgcolor: ConCreditoColor.GREEN }}>
                          <AttachFileIcon />
                        </Avatar>
                      </ListItemAvatar>
                      <ListItemText primary={doc.nombre} />
                    </ListItem>
                    <Divider variant="inset" component="li" />
                  </React.Fragment>
                ))}
              </List>
            </Grid>
          </Grid>
          <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
            <Button onClick={handleExit} sx={{ mt: 3, ml: 1 }} color="error">
              Salir
            </Button>
          </Box>
        </Paper>
      </Container>
      {showAutorizarDialog && <ConfirmDialog title="Autorizar" text="¿Desea autorizar al prospecto?" onCancel={() => setShowAutorizarDialog(false)} onConfirm={handleAutorizar} />}
      {showRechazarDialog && <RechazarDialog onCancel={() => setShowRechazarDialog(false)} onConfirm={handleRechazar} />}
    </React.Fragment>
  );
}

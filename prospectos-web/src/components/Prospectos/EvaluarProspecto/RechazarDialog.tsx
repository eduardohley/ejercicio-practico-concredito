import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { evaluarValidationSchema } from "./evaluarValidationSchema";
import { useFormik } from "formik";

type RechazarDialogProps = {
  onCancel: () => void;
  onConfirm: (observaciones: string) => void;
};

export function RechazarDialog({ onCancel, onConfirm }: RechazarDialogProps) {
  function handleSubmit({ observaciones }: any) {
    if (typeof onConfirm === "function") {
      onConfirm(observaciones.trim());
    }
  }

  const formik = useFormik({
    initialValues: {
      observaciones: "",
    },
    validationSchema: evaluarValidationSchema,
    onSubmit: handleSubmit,
  });

  return (
    <div>
      <Dialog open={true} onClose={onCancel}>
        <form onSubmit={formik.handleSubmit}>
          <DialogTitle>Rechazar</DialogTitle>
          <DialogContent>
            <DialogContentText>Al rechazar es necesario ingresar las observaciones</DialogContentText>
            <TextField
              id="observaciones"
              name="observaciones"
              autoFocus
              margin="dense"
              label="Observaciones"
              fullWidth
              variant="standard"
              value={formik.values.observaciones}
              onChange={formik.handleChange}
              error={formik.touched.observaciones && Boolean(formik.errors.observaciones)}
              helperText={formik.touched.observaciones && formik.errors.observaciones}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={onCancel} color="error">Cancelar</Button>
            <Button type="submit" color="success">Rechazar</Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
}

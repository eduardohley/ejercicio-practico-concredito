import * as yup from "yup";
import { YupMessages } from "../../../utils/yup/yup-messages";

export const evaluarValidationSchema = yup.object({
  observaciones: yup
    .string()
    .label("Observaciones")
    .min(10, YupMessages.min)
    .max(300, YupMessages.max)
    .required(YupMessages.required)
    .test("minLength", YupMessages.min({ label: "Observaciones", min: 10 }), (val) => (val || "").replace(/ +(?= )/g, "").length >= 10),
});

import { useEffect, useState } from "react";
import { Button, Grid, IconButton, TextField } from "@mui/material";
import { Box } from "@mui/system";
import RemoveCircleIcon from "@mui/icons-material/RemoveCircleOutline";
import AddIcon from "@mui/icons-material/Add";

type CapturarProspectoDocumentosProps = {
  onChange: (nombres: string[], documentos: File[]) => void;
};

export const CapturarProspectoDocumentos = ({ onChange }: CapturarProspectoDocumentosProps) => {
  const [nombres, setNombres] = useState<string[]>([]);
  const [documentos, setDocumentos] = useState<File[]>([]);

  function handleNombreChange(value: string, i: number) {
    const tmp = [...nombres];
    tmp[i] = value;
    setNombres(tmp);
  }

  function handleFilesChange(e: any) {
    const files = Array.from<File>(e.target.files).map((file: File) => file);
    setDocumentos([...documentos, ...files]);
    const nombresDocumentos = files.map((file) => file.name.substring(0, file.name.lastIndexOf(".")));
    setNombres([...nombres, ...nombresDocumentos]);
  }

  function handleRemoveFile(index: number) {
    setDocumentos(documentos.filter((file, i) => i !== index));
    setNombres(nombres.filter((nombre, i) => i !== index));
  }

  useEffect(() => {
    if (typeof onChange === "function") {
      onChange(nombres, documentos)
    }
  }, [nombres, documentos, onChange])

  return (
    <>
      <Grid item xs={12}>
        <Button fullWidth startIcon={<AddIcon />} variant="outlined" size="small" component="label">
          Agregar documentos
          <input onChange={handleFilesChange} multiple type="file" name="documentos" id="documentos" hidden />
        </Button>
      </Grid>
      <Grid item xs={12}>
        {nombres?.map((nombre, i) => (
          <Box key={i} sx={{ display: "flex", mb: 1 }}>
            <TextField
              sx={{ flex: 1 }}
              hiddenLabel
              value={nombre}
              onChange={({ target }) => {
                const value = target.value;
                handleNombreChange(value, i);
              }}
              size="small"
            />
            <IconButton tabIndex={-1} sx={{ ml: 1 }} onClick={() => handleRemoveFile(i)}>
              <RemoveCircleIcon color="error" />
            </IconButton>
          </Box>
        ))}
      </Grid>
    </>
  );
};

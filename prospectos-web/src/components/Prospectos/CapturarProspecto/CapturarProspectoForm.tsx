import React, { useState } from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import { Button, Container, Divider, LinearProgress, Paper } from "@mui/material";
import { useFormik } from "formik";
import { ServerException } from "../../../api/exception/dto/ServerException";
import { Box } from "@mui/system";
import NavigationPrompt from "react-router-navigation-prompt";
import { ConfirmDialog } from "../../core/ConfirmDialog";
import { useHistory } from "react-router-dom";
import { CreateProspectoRequestDto } from "../../../api/prospecto/dto/create-prospecto.dto";
import { Prospecto } from "../../../api/prospecto/prospecto";
import { prospectoValidationSchema } from "./prospectoValidationSchema";
import { CapturarProspectoDocumentos } from "./CapturarProspectoDocumentos";
import { DocumentoProspecto } from "../../../api/documento/documento-prospecto";
import LoadingButton from "@mui/lab/LoadingButton";
import SaveIcon from "@mui/icons-material/Save";
import { BasicDialog } from "../../core/BasicDialog";
import { ErrorAlert } from "../../core/ErrorAlert";
import { parseError } from "../../../utils/parseError";

export function CapturarProspectoForm() {
  const history = useHistory();
  const [nombres, setNombres] = useState<string[]>([]);
  const [documentos, setDocumentos] = useState<File[]>([]);
  const [error, setError] = useState<ServerException>();
  const [uploadProgress, setUploadProgress] = useState(0);
  const [saving, setSaving] = useState(false);
  const [showSavedDialog, setShowSavedDialog] = useState(false);

  function handleExit() {
    history.push("/prospectos");
  }

  function handleDocumentosChange(nombres: string[], documentos: File[]) {
    setNombres(nombres);
    setDocumentos(documentos);
  }

  function handleUploadProgress(progress: any) {
    const percent = Number(((progress.loaded * 100) / progress.total).toFixed(0));
    setUploadProgress(percent === 100 ? 0 : percent);
  }

  async function handleCreate(values: CreateProspectoRequestDto) {
    if (documentos.length === 0) {
      setError({ statusCode: 0, message: ["Debe agregar por lo menos un documento."] });
      return;
    }
    setError(undefined);
    setUploadProgress(0);
    setSaving(true);
    try {
      const prospecto = await Prospecto.create(values);

      if (prospecto) {
        await DocumentoProspecto.upload({ nombres, documentos, prospectoId: prospecto.id }, handleUploadProgress);
        formik.resetForm();
        setShowSavedDialog(true);
      }
    } catch (error: any) {
      setError(parseError(error));
    } finally {
      setSaving(false);
    }
  }

  const formik = useFormik({
    initialValues: {
      nombre: "",
      primerApellido: "",
      segundoApellido: "",
      telefono: "",
      rfc: "",
      calle: "",
      numero: "",
      colonia: "",
      codigoPostal: "",
    },
    validationSchema: prospectoValidationSchema,
    onSubmit: handleCreate,
  });

  return (
    <React.Fragment>
      <Container component="main" maxWidth="sm" sx={{ mb: 4 }}>
        <Typography component="h5" variant="h5" align="center">
          Capturar Prospecto
        </Typography>
        <Paper variant="outlined" sx={{ my: { xs: 3 }, p: { xs: 2 } }}>
          <form onSubmit={formik.handleSubmit}>
            <Grid container spacing={3} rowSpacing={2}>
              <Grid item xs={12}>
                <Typography variant="h6" gutterBottom>
                  Datos personales
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  id="nombre"
                  name="nombre"
                  label="Nombre"
                  fullWidth
                  variant="standard"
                  value={formik.values.nombre}
                  onChange={formik.handleChange}
                  error={formik.touched.nombre && Boolean(formik.errors.nombre)}
                  helperText={formik.touched.nombre && formik.errors.nombre}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  required
                  id="primerApellido"
                  name="primerApellido"
                  label="Primer apellido"
                  fullWidth
                  variant="standard"
                  value={formik.values.primerApellido}
                  onChange={formik.handleChange}
                  error={formik.touched.primerApellido && Boolean(formik.errors.primerApellido)}
                  helperText={formik.touched.primerApellido && formik.errors.primerApellido}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="segundoApellido"
                  name="segundoApellido"
                  label="Segundo apellido"
                  fullWidth
                  variant="standard"
                  value={formik.values.segundoApellido}
                  onChange={formik.handleChange}
                  error={formik.touched.segundoApellido && Boolean(formik.errors.segundoApellido)}
                  helperText={formik.touched.segundoApellido && formik.errors.segundoApellido}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  required
                  id="telefono"
                  name="telefono"
                  label="Teléfono"
                  fullWidth
                  autoComplete="shipping address-line2"
                  variant="standard"
                  value={formik.values.telefono}
                  onChange={formik.handleChange}
                  error={formik.touched.telefono && Boolean(formik.errors.telefono)}
                  helperText={formik.touched.telefono && formik.errors.telefono}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  required
                  id="rfc"
                  name="rfc"
                  label="RFC"
                  fullWidth
                  variant="standard"
                  value={formik.values.rfc}
                  onChange={formik.handleChange}
                  error={formik.touched.rfc && Boolean(formik.errors.rfc)}
                  helperText={formik.touched.rfc && formik.errors.rfc}
                />
              </Grid>
              <Grid item xs={12} sx={{ mt: 2 }}>
                <Typography variant="h6" gutterBottom>
                  Dirección
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <TextField
                  required
                  id="calle"
                  name="calle"
                  label="Calle"
                  fullWidth
                  variant="standard"
                  value={formik.values.calle}
                  onChange={formik.handleChange}
                  error={formik.touched.calle && Boolean(formik.errors.calle)}
                  helperText={formik.touched.calle && formik.errors.calle}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  required
                  id="numero"
                  name="numero"
                  label="Número"
                  fullWidth
                  variant="standard"
                  value={formik.values.numero}
                  onChange={formik.handleChange}
                  error={formik.touched.numero && Boolean(formik.errors.numero)}
                  helperText={formik.touched.numero && formik.errors.numero}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  required
                  id="colonia"
                  name="colonia"
                  label="Colonia"
                  fullWidth
                  variant="standard"
                  value={formik.values.colonia}
                  onChange={formik.handleChange}
                  error={formik.touched.colonia && Boolean(formik.errors.colonia)}
                  helperText={formik.touched.colonia && formik.errors.colonia}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  required
                  id="codigoPostal"
                  name="codigoPostal"
                  label="Código postal"
                  fullWidth
                  variant="standard"
                  value={formik.values.codigoPostal}
                  onChange={formik.handleChange}
                  error={formik.touched.codigoPostal && Boolean(formik.errors.codigoPostal)}
                  helperText={formik.touched.codigoPostal && formik.errors.codigoPostal}
                />
              </Grid>
              <Grid item xs={12} sx={{ mt: 2 }}>
                <Typography variant="h6" gutterBottom>
                  Documentos
                </Typography>
              </Grid>
              <CapturarProspectoDocumentos onChange={handleDocumentosChange} />
              {uploadProgress !== 0 && (
                <Grid item xs={12} sx={{ mt: 2 }}>
                  <Box sx={{ width: "100%" }}>
                    <LinearProgress variant="determinate" value={uploadProgress} />
                  </Box>
                </Grid>
              )}
              {error && (
                <>
                  <Divider sx={{ py: 1 }} />
                  <Grid item xs={12}>
                    <ErrorAlert error={error} />
                  </Grid>
                </>
              )}
            </Grid>
            <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
              {!saving && uploadProgress === 0 && (
                <Button onClick={handleExit} sx={{ mt: 3, ml: 1 }} color="error">
                  Salir
                </Button>
              )}
              <LoadingButton loading={uploadProgress > 0 || saving} loadingPosition="start" startIcon={<SaveIcon />} type="submit" variant="contained" sx={{ mt: 3, ml: 1 }}>
                Guardar
              </LoadingButton>
            </Box>
          </form>
        </Paper>
      </Container>
      <NavigationPrompt when={formik.dirty || uploadProgress > 0}>
        {({ onConfirm, onCancel }) => <ConfirmDialog title="¿Está seguro de salir?" text="Ningún dato será guardado." onCancel={onCancel} onConfirm={onConfirm} />}
      </NavigationPrompt>
      {showSavedDialog && <BasicDialog title="Guardar" text="Prospecto guardado exitosamente" onConfirm={handleExit} />}
    </React.Fragment>
  );
}

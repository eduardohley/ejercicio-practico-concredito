import * as yup from "yup";
import { YupMessages } from "../../../utils/yup/yup-messages";

export const prospectoValidationSchema = yup.object({
  nombre: yup.string().label("Nombre").min(1, YupMessages.min).max(50, YupMessages.max).required(YupMessages.required),
  primerApellido: yup.string().label("Primer apellido").min(1, YupMessages.min).max(50, YupMessages.max).required(YupMessages.required),
  segundoApellido: yup.string().label("Segundo apellido").max(50, YupMessages.max),
  telefono: yup
    .string()
    .matches(/^[0-9]+$/, ({ label }) => `El campo ${label} debe contener solo números`)
    .label("Teléfono")
    .min(6, YupMessages.min)
    .max(15, YupMessages.max)
    .typeError(YupMessages.typeError)
    .required(YupMessages.required),
  rfc: yup.string().label("RFC").min(12, YupMessages.min).max(13, YupMessages.max).required(YupMessages.required),
  calle: yup.string().label("Calle").min(1, YupMessages.min).max(50, YupMessages.max).required(YupMessages.required),
  numero: yup.string().label("Número").min(1, YupMessages.min).max(10, YupMessages.max).required(YupMessages.required),
  colonia: yup.string().label("Colonia").min(1, YupMessages.min).max(50, YupMessages.max).required(YupMessages.required),
  codigoPostal: yup.string().label("Código postal").min(1, YupMessages.min).max(20, YupMessages.max).required(YupMessages.required),
});

import { Route, Switch, useRouteMatch } from "react-router-dom";
import { Rol } from "../../api/auth/rol.enum";
import PrivateRoute from "../core/PrivateRoute";
import { CapturarProspectoForm } from "./CapturarProspecto/CapturarProspectoForm";
import { DetallesProspecto } from "./DetallesProspecto";
import { EvaluarProspecto } from "./EvaluarProspecto";
import { ListaProspectos } from "./ListaProspectos";

const ProspectosRoutes = () => {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <Route path={path} exact>
        <ListaProspectos />
      </Route>
      <PrivateRoute path={`${path}/capturar-prospecto`} component={CapturarProspectoForm} roles={[Rol.PROMOTOR_PROSPECTOS]} />
      <PrivateRoute path={`${path}/detalles-prospecto/:prospectoId`} component={DetallesProspecto} roles={[Rol.PROMOTOR_PROSPECTOS]} />
      <PrivateRoute path={`${path}/evaluar-prospecto/:prospectoId`} component={EvaluarProspecto} roles={[Rol.EVALUADOR_PROSPECTOS]} />
    </Switch>
  );
};

export { ProspectosRoutes };

import { Button, Chip, Divider, Grid, IconButton, Stack, Toolbar, Typography } from "@mui/material";
import { grey } from "@mui/material/colors";
import { Box } from "@mui/system";
import { DataGrid, GridColDef, esES } from "@mui/x-data-grid";
import { useEffect, useState } from "react";
import { ServerException } from "../../../api/exception/dto/ServerException";
import { GetProspectoResponseDto } from "../../../api/prospecto/dto/get-prospecto.dto";
import { Prospecto } from "../../../api/prospecto/prospecto";
import { DataGridLoadingOverlay } from "../../core/DataGridLoadingOverlay";
import RefreshIcon from "@mui/icons-material/Refresh";
import AddIcon from "@mui/icons-material/Add";
import InfoIcon from "@mui/icons-material/Info";
import DoNotDisturbIcon from "@mui/icons-material/DoNotDisturb";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import PendingIcon from "@mui/icons-material/Pending";
import PrivateComponent from "../../core/PrivateComponent";
import { Rol } from "../../../api/auth/rol.enum";
import { ToolbarSearch } from "./ToolbarSearch";
import { Link, useRouteMatch } from "react-router-dom";
import { EstadoProspecto } from "../../../api/prospecto/dto/estado-prospecto.enum";
import FactCheckIcon from "@mui/icons-material/FactCheck";
import { ErrorAlert } from "../../core/ErrorAlert";
import { parseError } from "../../../utils/parseError";

const columns: GridColDef[] = [
  {
    field: "nombre",
    headerName: "Nombre",
    width: 250,
  },
  {
    field: "primerApellido",
    headerName: "Primer apellido",
    width: 250,
  },
  {
    field: "segundoApellido",
    headerName: "Segundo apellido",
    width: 250,
  },
  {
    field: "estado",
    headerName: "Estado",
    width: 150,
  },
  {
    field: "estado",
    headerName: "Estatus",
    sortable: false,
    renderCell: (params: any) => {
      const { value: estatus } = params;
      if (estatus === EstadoProspecto.Enviado) {
        return  <Chip label={estatus} color="info" icon={<PendingIcon />} />
      }
      if (estatus === EstadoProspecto.Autorizado) {
        return  <Chip label={estatus} color="success" icon={<CheckCircleOutlineIcon />} />
      }
      if (estatus === EstadoProspecto.Rechazado) {
        return  <Chip label={estatus} color="error" icon={<DoNotDisturbIcon />} />
      }
    }
  },
];

type DataGridRow = {
  id: number;
  nombre: string;
  primerApellido: string;
  segundoApellido: string;
  estado: string;
};

const ListaProspectos = () => {
  const { path } = useRouteMatch();
  const [error, setError] = useState<ServerException | undefined>();
  const [loading, setLoading] = useState(false);
  const [prospectos, setProspectos] = useState<GetProspectoResponseDto[]>([]);
  const [rows, setRows] = useState<DataGridRow[]>([]);
  const [selection, setSelection] = useState<number[]>();
  const [selectedProspectoId, setSelectedProspectoId] = useState<number>();

  useEffect(() => {
    if (selection && selection.length > 0) {
      const [id] = selection;
      setSelectedProspectoId(id);
    }
  }, [selection]);

  function handleSearch(value: string) {
    const filterProspecto = prospectos.filter((p) => [p.nombre, p.primerApellido, p.segundoApellido, p.estado?.toString()].some((prop) => prop?.includes(value)));
    setRows(filterProspecto.map(({ id, nombre, primerApellido, segundoApellido, estado }) => ({ id, nombre, primerApellido, segundoApellido, estado: estado as EstadoProspecto })));
  }

  async function loadProspectos() {
    setLoading(true);
    try {
      const prospectos = await Prospecto.findMany();
      setProspectos(prospectos);
    } catch (error: any) {
      setError(parseError(error));
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    loadProspectos();
  }, []);

  useEffect(() => {
    if (prospectos) {
      setRows(prospectos.map(({ id, nombre, primerApellido, segundoApellido, estado }) => ({ id, nombre, primerApellido, segundoApellido, estado: estado as EstadoProspecto })));
    }
  }, [prospectos]);

  return (
    <div style={{ height: "calc(100vh - 178px)", width: "100%" }}>
      {error && (
        <>
          <Divider sx={{ my: 1 }} />
          <ErrorAlert error={error} />
        </>
      )}
      <Box sx={{ my: 1, border: `1px solid ${grey[300]}`, borderRadius: "4px" }}>
        <Toolbar variant="dense" sx={{ px: [1] }}>
          <Grid container direction="row" justifyContent="flex-end" gap={2}>
            <Stack direction="row" spacing={1}>
              {!selection && (
                <Typography variant="caption" display="block" gutterBottom sx={{ m: 0, display: "flex", alignItems: "center" }}>
                  Seleccione un registro para ver acciones disponibles
                </Typography>
              )}
              {selection && selection.length > 0 && (
                <>
                  <PrivateComponent roles={[Rol.PROMOTOR_PROSPECTOS]}>
                    <Link to={`${path}/detalles-prospecto/${selectedProspectoId}`} style={{ textDecoration: "none" }}>
                      <Button startIcon={<InfoIcon />} color="info" variant="outlined" size="small">
                        Detalles
                      </Button>
                    </Link>
                  </PrivateComponent>
                  <PrivateComponent roles={[Rol.EVALUADOR_PROSPECTOS]}>
                    <Link to={`${path}/evaluar-prospecto/${selectedProspectoId}`} style={{ textDecoration: "none" }}>
                      <Button startIcon={<FactCheckIcon />} color="secondary" variant="outlined" size="small">
                        Evaluar
                      </Button>
                    </Link>
                  </PrivateComponent>
                </>
              )}
            </Stack>
            <Grid item>
              <ToolbarSearch onChange={handleSearch} />
            </Grid>
            <PrivateComponent roles={[Rol.PROMOTOR_PROSPECTOS]}>
              <Grid item>
                <Link to={`${path}/capturar-prospecto`} style={{ textDecoration: "none" }}>
                  <Button startIcon={<AddIcon />} variant="contained" color="success" size="small">
                    Nuevo prospecto
                  </Button>
                </Link>
              </Grid>
            </PrivateComponent>
            <Grid item>
              <IconButton onClick={loadProspectos} size="small">
                <RefreshIcon />
              </IconButton>
            </Grid>
          </Grid>
        </Toolbar>
      </Box>
      <DataGrid
        onSelectionModelChange={(selection: any) => {
          setSelection(selection);
        }}
        selectionModel={selection}
        rows={rows}
        columns={columns}
        pageSize={8}
        rowsPerPageOptions={[8]}
        localeText={esES.components.MuiDataGrid.defaultProps.localeText}
        components={{
          LoadingOverlay: DataGridLoadingOverlay,
        }}
        loading={loading}
      />
    </div>
  );
};

export { ListaProspectos };

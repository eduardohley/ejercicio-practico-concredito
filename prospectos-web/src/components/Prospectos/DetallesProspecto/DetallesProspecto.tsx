import React, { useEffect, useState } from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import { Avatar, Button, Card, CardContent, Chip, Collapse, Container, Divider, IconButton, List, ListItem, ListItemAvatar, ListItemText, Paper, Stack } from "@mui/material";
import { ServerException } from "../../../api/exception/dto/ServerException";
import { Box } from "@mui/system";
import { useHistory, useParams } from "react-router-dom";
import { Prospecto } from "../../../api/prospecto/prospecto";
import PendingIcon from "@mui/icons-material/Pending";
import DoNotDisturbIcon from "@mui/icons-material/DoNotDisturb";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import OpenInNewIcon from "@mui/icons-material/OpenInNew";
import AttachFileIcon from "@mui/icons-material/AttachFile";
import { GetProspectoResponseDto } from "../../../api/prospecto/dto/get-prospecto.dto";
import { ConCreditoColor } from "../../../theme/colors";
import { EstadoProspecto } from "../../../api/prospecto/dto/estado-prospecto.enum";
import { baseURL } from "../../../config/server";
import { ErrorAlert } from "../../core/ErrorAlert";
import { parseError } from "../../../utils/parseError";

const initialProspecto: GetProspectoResponseDto = {
  id: 0,
  nombre: "",
  primerApellido: "",
  segundoApellido: "",
  telefono: "",
  rfc: "",
  calle: "",
  numero: "",
  colonia: "",
  codigoPostal: "",
  documentos: [],
};

export function DetallesProspecto() {
  const { prospectoId } = useParams<{ prospectoId: string }>();
  const history = useHistory();
  const [prospecto, setProspecto] = useState<GetProspectoResponseDto>(initialProspecto);
  const [error, setError] = useState<ServerException>();
  const [collapseObservaciones, setCollapseObservaciones] = useState(false);

  function handleExit() {
    history.push("/prospectos");
  }

  async function loadProspecto() {
    try {
      const prospecto = await Prospecto.findById(Number(prospectoId));
      if (prospecto) {
        setProspecto(prospecto);
      } else {
        setProspecto(initialProspecto);
      }
    } catch (error: any) {
      setError(parseError(error));
    }
  }

  useEffect(() => {
    loadProspecto();
  }, []);

  return (
    <React.Fragment>
      <Typography component="h5" variant="h5" align="center">
        Detalles de Prospecto
      </Typography>
      <Container component="main" maxWidth="sm" sx={{ mb: 4 }}>
        <Paper variant="outlined" sx={{ my: { xs: 3 }, p: { xs: 2 } }}>
          <Grid container spacing={3} rowSpacing={2}>
            {error && (
              <Grid item xs={12}>
                <ErrorAlert error={error} />
              </Grid>
            )}
            <Grid item xs={6}>
              <Typography variant="h6" gutterBottom>
                Datos personales
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Stack direction="row" spacing={1}>
                {prospecto.estado === EstadoProspecto.Enviado && <Chip label="Enviado" color="info" icon={<PendingIcon />} />}
                {prospecto.estado === EstadoProspecto.Autorizado && <Chip label="Autorizado" color="success" icon={<CheckCircleOutlineIcon />} />}
                {prospecto.estado === EstadoProspecto.Rechazado && (
                  <Chip
                    onClick={() => setCollapseObservaciones(!collapseObservaciones)}
                    label="Rechazado"
                    color="error"
                    icon={<DoNotDisturbIcon />}
                    deleteIcon={collapseObservaciones ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                    onDelete={() => setCollapseObservaciones(!collapseObservaciones)}
                  />
                )}
              </Stack>
            </Grid>
            <Grid item xs={12}>
              <Collapse in={collapseObservaciones}>
                <Card sx={{ backgroundColor: "grey" }}>
                  <CardContent>
                    <Typography variant="h6">Observaciones</Typography>
                    <Typography variant="body2" color="text.secondary">
                      {prospecto.observaciones}
                    </Typography>
                  </CardContent>
                </Card>
              </Collapse>
            </Grid>
            <Grid item xs={12}>
              <TextField
                label="Nombre"
                fullWidth
                variant="standard"
                value={prospecto.nombre}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="Primer apellido"
                fullWidth
                variant="standard"
                value={prospecto.primerApellido}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="Segundo apellido"
                fullWidth
                variant="standard"
                value={prospecto.segundoApellido}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="Teléfono"
                fullWidth
                autoComplete="shipping address-line2"
                variant="standard"
                value={prospecto.telefono}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="RFC"
                fullWidth
                variant="standard"
                value={prospecto.rfc}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={12} sx={{ mt: 2 }}>
              <Typography variant="h6" gutterBottom>
                Dirección
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="Calle"
                fullWidth
                variant="standard"
                value={prospecto.calle}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="Número"
                fullWidth
                variant="standard"
                value={prospecto.numero}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="Colonia"
                fullWidth
                variant="standard"
                value={prospecto.colonia}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                label="Código postal"
                fullWidth
                variant="standard"
                value={prospecto.codigoPostal}
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item xs={12} sx={{ mt: 2 }}>
              <Typography variant="h6" gutterBottom>
                Documentos
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <List dense={true}>
                {prospecto.documentos.map((doc, i) => (
                  <React.Fragment key={i}>
                    <ListItem
                      secondaryAction={
                        <IconButton edge="end" aria-label="delete" target="_blank" href={baseURL + doc.archivo}>
                          <OpenInNewIcon />
                        </IconButton>
                      }
                    >
                      <ListItemAvatar>
                        <Avatar sx={{ bgcolor: ConCreditoColor.GREEN }}>
                          <AttachFileIcon />
                        </Avatar>
                      </ListItemAvatar>
                      <ListItemText primary={doc.nombre} />
                    </ListItem>
                    <Divider variant="inset" component="li" />
                  </React.Fragment>
                ))}
              </List>
            </Grid>
          </Grid>
          <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
            <Button onClick={handleExit} sx={{ mt: 3, ml: 1 }} color="error">
              Salir
            </Button>
          </Box>
        </Paper>
      </Container>
    </React.Fragment>
  );
}

import { Route, Switch } from "react-router-dom";
import { Inicio } from "./Inicio/Inicio";
import { ProspectosRoutes } from "./Prospectos/ProspectosRoutes";

const AppRoutes = () => {
  return (
    <Switch>
      <Route path="/" exact>
        <Inicio />
      </Route>
      <Route path="/prospectos">
        <ProspectosRoutes />
      </Route>
    </Switch>
  );
};

export { AppRoutes };

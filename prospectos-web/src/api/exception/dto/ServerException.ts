export type ServerException = {
  statusCode: number;
  message: string[];
  error?: string;
}
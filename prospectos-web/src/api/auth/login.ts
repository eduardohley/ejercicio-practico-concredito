import { writeStorage, deleteFromStorage } from "@rehooks/local-storage";
import { requestAPI } from "../requestAPI";

namespace Auth {
  export function login(username: string, password: string) {
    return requestAPI({
      method: "POST",
      url: "/auth/login",
      data: {
        username,
        password,
      },
    });
  }

  export function logout() {
    deleteFromStorage("token");
  }

  export function setToken(token: string) {
    writeStorage("token", token);
  }

  export function getToken(): string | null {
    return localStorage.getItem("token") as string;
  }
}

export { Auth };

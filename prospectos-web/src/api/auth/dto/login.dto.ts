export interface PostLoginRequestDto {
  username: string;
  password: string;
}
import { requestAPI } from "../requestAPI";
import { CreateProspectoRequestDto, CreateProspectoResponseDto } from "./dto/create-prospecto.dto";
import { GetProspectoResponseDto } from "./dto/get-prospecto.dto";
import { UpdateProspectoRequestDto, UpdateProspectoResponseDto } from "./dto/update-prospecto.dto";

namespace Prospecto {
  export async function findMany(): Promise<GetProspectoResponseDto[]> {
    const { data } = await requestAPI({
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      url: "/prospecto",
    });

    return data as GetProspectoResponseDto[];
  }

  export async function create(prospecto: CreateProspectoRequestDto): Promise<CreateProspectoResponseDto> {
    const { data } = await requestAPI({
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      url: "/prospecto",
      data: prospecto,
    });

    return data as CreateProspectoResponseDto;
  }

  export async function findById(prospectoId: number): Promise<GetProspectoResponseDto> {
    const { data } = await requestAPI({
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      url: `/prospecto/${prospectoId}`,
    });

    return data as GetProspectoResponseDto;
  }

  export async function update(prospectoId: number, prospecto: UpdateProspectoRequestDto): Promise<UpdateProspectoResponseDto> {
    const { data } = await requestAPI({
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      url: "/prospecto/" + prospectoId,
      data: prospecto,
    });

    return data as UpdateProspectoResponseDto;
  }
}

export { Prospecto };

export enum EstadoProspecto {
  Enviado = "Enviado",
  Autorizado = "Autorizado",
  Rechazado = "Rechazado",
}

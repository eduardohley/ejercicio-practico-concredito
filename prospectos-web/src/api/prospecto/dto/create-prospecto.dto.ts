import { ProspectoDto } from "./prospecto.dto";
import { Omit } from "../../../utils/class-omit";

export class CreateProspectoResponseDto extends ProspectoDto {}

export class CreateProspectoRequestDto extends Omit(ProspectoDto, ["id", "documentos"]) {}

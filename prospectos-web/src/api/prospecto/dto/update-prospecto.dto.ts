import { EstadoProspecto } from "./estado-prospecto.enum";
import { ProspectoDto } from "./prospecto.dto";

export class UpdateProspectoRequestDto {
  nombre?: string;
  primerApellido?: string;
  segundoApellido?: string;
  telefono?: string;
  rfc?: string;
  calle?: string;
  numero?: string;
  colonia?: string;
  codigoPostal?: string;
  estado?: EstadoProspecto;
  observaciones?: string;
}

export class UpdateProspectoResponseDto extends ProspectoDto {}
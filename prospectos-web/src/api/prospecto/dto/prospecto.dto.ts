import { DocumentoProspectoDto } from "../../documento/dto/documento.dto";
import { EstadoProspecto } from "./estado-prospecto.enum";

class ProspectoDto {
  id: number;
  nombre: string;
  primerApellido: string;
  segundoApellido: string;
  telefono: string;
  rfc: string;
  calle: string;
  numero: string;
  colonia: string;
  codigoPostal: string;
  estado?: EstadoProspecto;
  observaciones?: string;
  documentos: DocumentoProspectoDto[];
}

export { ProspectoDto };

import axios, { AxiosRequestConfig } from "axios";
import { baseURL } from "../config/server";

const defaultOptions: AxiosRequestConfig = {
  baseURL: baseURL + "/api",
};

const requestAPI = axios.create(defaultOptions);

requestAPI.interceptors.request.use(function (config) {
  const token = localStorage.getItem("token");
  config.headers = { ...config.headers, Authorization: token ? `Bearer ${token}` : "" };
  return config;
});

export { requestAPI };

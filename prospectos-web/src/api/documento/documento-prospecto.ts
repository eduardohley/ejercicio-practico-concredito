import { requestAPI } from "../requestAPI";
import { CreateDocumentoProspectoRequestDto, CreateDocumentoProspectoResponseDto } from "./dto/create-documento-prospecto.dto";

namespace DocumentoProspecto {
  export async function upload(documentoProspectoDto: CreateDocumentoProspectoRequestDto, onUploadProgress: (progress: any) => void): Promise<CreateDocumentoProspectoResponseDto> {
    const formData = new FormData();

    formData.append("prospectoId", documentoProspectoDto.prospectoId.toString());
    
    for (const file of documentoProspectoDto.documentos) {
      formData.append("documentos", file);
    }
    for (const nombre of documentoProspectoDto.nombres) {
      formData.append("nombres", nombre);
    }

    const { data } = await requestAPI({
      method: "POST",
      headers: {
        "Content-Type": "multipart/form-data",
      },
      url: "/documento-prospecto",
      data: formData,
      onUploadProgress
    });

    return data as CreateDocumentoProspectoResponseDto;
  }
}

export { DocumentoProspecto };

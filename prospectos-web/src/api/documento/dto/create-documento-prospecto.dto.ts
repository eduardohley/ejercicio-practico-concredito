export class CreateDocumentoProspectoRequestDto {
  prospectoId: number;
  documentos: File[];
  nombres: string[];
}

export class CreateDocumentoProspectoResponseDto {
  count: number;
}
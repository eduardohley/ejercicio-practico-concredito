class DocumentoProspectoDto {
  id: number;
  nombre: string;
  archivo: string;
  prospectoId: number;
}

export { DocumentoProspectoDto }
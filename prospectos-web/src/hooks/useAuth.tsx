import useLocalStorage, { deleteFromStorage } from "@rehooks/local-storage";
import jwt from "jwt-decode";
import { useEffect, useState } from "react";
import { JWTPayload } from "../api/auth/dto/jwt";

export function useAuth(): [JWTPayload | undefined, () => void] {
  const [token] = useLocalStorage<string>("token");
  const [usuario, setUsuario] = useState<JWTPayload | undefined>(decodeToken(token));

  function decodeToken(token: string | null): JWTPayload | undefined {
    if (!token) {
      return undefined;
    }
    return jwt<JWTPayload>(token) 
  }

  function logout() {
    deleteFromStorage("token");
    setUsuario(undefined);
  }

  useEffect(() => {
    if (token) {
      const decodedToken = decodeToken(token)
      setUsuario(decodedToken);
    } else {
      setUsuario(undefined);
    }
  }, [token]);

  return [usuario, logout];
}

import { ThemeProvider } from "@mui/material";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import PrivateRoute from "./components/core/PrivateRoute";
import { Layout } from "./components/Layout";
import Login from "./components/Login/Login";
import { appTheme } from "./theme";

function App() {
  return (
    <ThemeProvider theme={appTheme}>
      <Router>
        <Switch>
          <Route path="/login" exact >
            <Login />
          </Route>
          <PrivateRoute path="/" component={Layout}></PrivateRoute>
          <Redirect to="/asd" />
        </Switch>
      </Router>
    </ThemeProvider>
  );
}

export default App;

import { createTheme } from "@mui/material";
import { grey } from "@mui/material/colors";
import { ConCreditoColor } from "./colors";

const appTheme = createTheme({
  components: {
    MuiAppBar: {
      styleOverrides: {
        colorPrimary: { backgroundColor: ConCreditoColor.GREEN },
      },
    },
    MuiLinearProgress: {
      styleOverrides: {
        bar: {
          backgroundColor: ConCreditoColor.GREEN,
        },
        colorPrimary: {
          backgroundColor: grey[400],
        },
      },
    },
  },
});

export { appTheme };

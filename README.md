
# Ejercicio Práctico ConCrédito

Para iniciar sistema con docker, ejecutar el siguiente comando dentro de la carpeta:

```docker-compose up```

***Para iniciar cada proyecto independiente fuera de docker,
vea los archivos de configuración al final de este README.***

Debería iniciar Mysql, NestJS y React.

Los puertos usados en los contenedores son:

Contenedor | Puerto | Url
--- | --- | ---
Mysql | 3307 | http://localhost:3307
Backend | 3333 | http://localhost:3333/api 
Frontend | 1001 | http://localhost:1001

***La documentación de Swagger para la API se encuentra en
http://localhost:3333/docs***

Acceso al sistema
Nombre de usuario | contraseña
--- | ---
promotor | promotor123
evaluador | evaluador123



Si tiene conflictos con los puertos, se pueden configurar en los siguientes
***archivos de configuración***:

- Mysql
    - db/.env
    - docker-compose.yml service.db ports

- Backend
    - prospectos-backend/.env
    - docker-compose.yml service.backend ports

- Frontend
    - prospectos-web/.env
    - docker-compose.yml service.frontend ports
